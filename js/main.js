(function () {
    //variables
    const lista = document.getElementById("lista"),
        tareaInput = document.getElementById("tareaInput"),
        formulario = document.getElementById("formulario"),
        pendientes = document.getElementById("pendiente");

    formulario.addEventListener("submit", function (evento) {
        evento.preventDefault();

        const texto = tareaInput.value;
        agregarTarea(texto);
        event.target.firstElementChild.value = "";
    });


    //funciones
    const agregarTarea = (texto) => {
        let tarea = tareaInput.value,
            nuevaTarea = document.createElement("li"),
            enlace = document.createElement("a"),
            contenido = document.createTextNode(tarea);



        if (tarea === "") {
            tareaInput.setAttribute("placeholder", "Agrega una tarea valida");
            tareaInput.className = "error";
            return false;
        }

        //Agragamos el contenido al enlace
        enlace.appendChild(contenido);
        //le establecemos un atributo href
        enlace.setAttribute("href", "#");


        tareaInput.value = "";


        enlace.addEventListener("click", function () {
            this.parentNode.remove();
            ModificarPendientes(lista.children.length + " Tareas pendientes");


        });

        //Agregamos el enlace(a) a la nueva tarea(li)
        nuevaTarea.appendChild(enlace);
        //agragamos la nueva tarea a lista
        lista.appendChild(nuevaTarea);
        ModificarPendientes(lista.children.length + " Tareas pendientes");



    }

    const ModificarPendientes = (listaLength) => {
        pendientes.innerText = listaLength;
    };

    const comprobarInput = () => {
        tareaInput.className = "";
        tareaInput.setAttribute("placeholder", "Agrega tu tarea");

    };

    //Agregar tareas

    //formulario.addEventListener("submit", agregarTarea);
    //comprobar input
    tareaInput.addEventListener("click", comprobarInput);

}());